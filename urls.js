
module.exports = {
  local: 'http://localhost:3000/',
  prod: 'https://cra-demo-prod.herokuapp.com/',
  dev: 'https://cra-demo-dev.herokuapp.com/'
}